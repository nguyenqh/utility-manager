class UsersController < ApplicationController
  def new
    @page_title = 'Add New User'
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    @user.save

    redirect_to users_path
  end

  def update
  end

  def edit
  end

  def destroy
  end

  def delete
  end

  def index
  end

  def show
    @readings = Reading.where(user_id: 1)
  end

  private
    def user_params
    params.require(:user).permit(:first_name, :last_name, :address)
  end
end
