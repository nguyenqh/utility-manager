class ReadingsController < ApplicationController
  def new
    @page_title = 'Add New Reading'
    @reading = Reading.new
    @user = User.new
  end

  def create
    @reading = Reading.new(reading_params)
    @reading.save

    redirect_to readings_path
  end

  def update
  end

  def edit
  end

  def destroy
  end

  def delete
  end

  def index
  end

  def show    
  end

  private
    def reading_params
    params.require(:reading).permit(:read_at, :user_id, :value)
  end
end
