class CreateReadings < ActiveRecord::Migration
  def change
    create_table :readings do |t|
      t.datetime :read_at
      t.integer :user_id
      t.integer :value

      t.timestamps null: false
    end
  end
end
